package com.demo.news.domain.listeners

interface OnItemClickListener<T> {

    fun onItemClick(item: T)

}