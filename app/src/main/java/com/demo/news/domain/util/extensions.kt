package com.demo.news.domain.util

import java.text.SimpleDateFormat
import java.util.*

const val PATTERN_FROM = "yyyy-MM-dd'T'HH:mm:ss'Z'"
const val PATTERN_TO = "MMMM dd, yyyy"

fun String.convertDate(
    fromPattern: String = PATTERN_FROM,
    toPattern: String = PATTERN_TO
): String = try {
    val parser = SimpleDateFormat(fromPattern, Locale.US)
    val formatter = SimpleDateFormat(toPattern, Locale.getDefault())
    formatter.format(parser.parse(this)!!)
} catch (e: Exception) {
    this
}