package com.demo.news.data.api.response

import com.demo.news.data.model.Article

data class GetArticlesResponse(
    val status: String,
    val message: String,
    val articles: List<Article>
)