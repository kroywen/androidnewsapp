package com.demo.news.data.api

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.demo.news.data.model.Article

class NewsPagingSource(
    private val apiService: NewsApiService
) : PagingSource<Int, Article>() {

    companion object {
        const val STATUS_OK = "ok"
        const val INITIAL_PAGE = 1
        const val PAGE_SIZE = 20
    }

    override fun getRefreshKey(state: PagingState<Int, Article>): Int? {
        val anchorPosition = state.anchorPosition ?: return null
        val page = state.closestPageToPosition(anchorPosition) ?: return null
        return page.prevKey?.plus(1) ?: page.nextKey?.minus(1)
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Article> = try {
        val page = params.key ?: INITIAL_PAGE
        val pageSize = params.loadSize.coerceAtMost(PAGE_SIZE)
        val response = apiService.getArticles(page, pageSize)

        if (response.status == STATUS_OK) {
            val articles = response.articles
            val nextKey = if (articles.size < pageSize) null else page + 1
            LoadResult.Page(articles, null, nextKey)
        } else {
            LoadResult.Error(Exception(response.message))
        }
    } catch (e: Exception) {
        LoadResult.Error(e)
    }

}