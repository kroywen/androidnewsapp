package com.demo.news.data.api

import com.demo.news.data.api.response.GetArticlesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApiService {

    @GET("everything?domains=techcrunch.com&sortBy=popularity&language=en")
    suspend fun getArticles(
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int
    ): GetArticlesResponse

}