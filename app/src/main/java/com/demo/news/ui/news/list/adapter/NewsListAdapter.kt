package com.demo.news.ui.news.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import com.demo.news.data.model.Article
import com.demo.news.databinding.CellNewsHeaderBinding
import com.demo.news.databinding.CellNewsItemBinding
import com.demo.news.domain.listeners.OnItemClickListener

class NewsListAdapter : PagingDataAdapter<Article, RecyclerView.ViewHolder>(NewsDiffCallback()) {

    companion object {
        const val TYPE_HEADER = 0
        const val TYPE_ITEM = 1
    }

    var listener: OnItemClickListener<Article>? = null

    override fun getItemViewType(position: Int) = when (position) {
        0 -> TYPE_HEADER
        else -> TYPE_ITEM
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            TYPE_HEADER -> NewsHeaderViewHolder(
                CellNewsHeaderBinding.inflate(inflater, parent, false),
                listener
            )
            else -> NewsItemViewHolder(
                CellNewsItemBinding.inflate(inflater, parent, false),
                listener
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val article = getItem(position)
        when (position) {
            0 -> (holder as NewsHeaderViewHolder).bind(article)
            else -> (holder as NewsItemViewHolder).bind(article)
        }
    }

}