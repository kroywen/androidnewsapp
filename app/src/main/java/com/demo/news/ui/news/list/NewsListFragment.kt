package com.demo.news.ui.news.list

import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import com.demo.news.R
import com.demo.news.data.model.Article
import com.demo.news.databinding.FragmentNewsListBinding
import com.demo.news.domain.listeners.OnItemClickListener
import com.demo.news.ui.base.BaseFragment
import com.demo.news.ui.news.list.adapter.NewsListAdapter
import com.zhuinden.fragmentviewbindingdelegatekt.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NewsListFragment : BaseFragment(R.layout.fragment_news_list) {

    private val binding by viewBinding(FragmentNewsListBinding::bind)
    private val viewModel by activityViewModels<NewsListViewModel>()
    private val adapter by lazy(LazyThreadSafetyMode.NONE) {
        NewsListAdapter()
    }

    override fun initViews() = with(binding) {
        recyclerView.adapter = adapter.apply {
            listener = object : OnItemClickListener<Article> {
                override fun onItemClick(item: Article) {
                    findNavController().navigate(
                        NewsListFragmentDirections.actionNewsListFragmentToNewsDetailsFragment(item.url)
                    )
                }
            }
        }
        adapter.addLoadStateListener { state ->
            recyclerView.isVisible = state.refresh != LoadState.Loading
            progressView.isVisible = state.refresh == LoadState.Loading
        }
    }

    override fun initViewModel() {
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.articles.collectLatest(adapter::submitData)
            }
        }
    }

}