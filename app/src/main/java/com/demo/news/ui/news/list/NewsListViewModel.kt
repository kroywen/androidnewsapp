package com.demo.news.ui.news.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.demo.news.data.api.NewsApiService
import com.demo.news.data.api.NewsPagingSource
import com.demo.news.data.model.Article
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject

@HiltViewModel
class NewsListViewModel @Inject constructor(private val apiService: NewsApiService) : ViewModel() {

    val articles: StateFlow<PagingData<Article>> = Pager(
        config = PagingConfig(pageSize = NewsPagingSource.PAGE_SIZE),
        pagingSourceFactory = {
            NewsPagingSource(apiService)
        })
        .flow
        .cachedIn(viewModelScope)
        .stateIn(viewModelScope, SharingStarted.Lazily, PagingData.empty())

}