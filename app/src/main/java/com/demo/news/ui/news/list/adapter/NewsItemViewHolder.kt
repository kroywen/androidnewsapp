package com.demo.news.ui.news.list.adapter

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.demo.news.R
import com.demo.news.data.model.Article
import com.demo.news.databinding.CellNewsItemBinding
import com.demo.news.domain.listeners.OnItemClickListener
import com.demo.news.domain.util.convertDate

class NewsItemViewHolder(
    private val binding: CellNewsItemBinding,
    private val listener: OnItemClickListener<Article>?
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(article: Article?) = with(binding) {
        article?.let {
            if (article.urlToImage.isNotEmpty()) {
                Glide.with(itemView.context).load(article.urlToImage).into(imageView)
            } else {
                Glide.with(itemView.context).load(R.drawable.image_placeholder).into(imageView)
            }
            titleView.text = article.title
            descriptionView.text = article.description
            dateView.text = article.publishedAt.convertDate()

            itemView.setOnClickListener {
                listener?.onItemClick(article)
            }
        }
    }

}