package com.demo.news.ui.news.details

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import androidx.navigation.fragment.navArgs
import com.demo.news.R
import com.demo.news.databinding.FragmentNewsDetailsBinding
import com.demo.news.ui.base.BaseFragment
import com.zhuinden.fragmentviewbindingdelegatekt.viewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NewsDetailsFragment : BaseFragment(R.layout.fragment_news_details) {

    private val binding by viewBinding(FragmentNewsDetailsBinding::bind)
    private val args: NewsDetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.webView.loadUrl(args.url)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun initViews() = with(binding) {
        webView.settings.run {
            javaScriptEnabled = true
            domStorageEnabled = true
        }
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                progressView.isVisible = true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                progressView.isVisible = false
            }
        }
    }

}